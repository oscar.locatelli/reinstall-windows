# INSTALLAZIONE POSH GIT, OH-MY-POSH e PSREADLINE

### Personalizzazione Windows Terminal Preview

- Apri il Windows Terminal Preview
- Cambia la shell predefinita dalle impostazioni nella tua WSL2 e l'aspetto con il tema preferito
- Installa la versione Nerd Font di Cascadia Code, CaskaydiaCove NF

```sh
scoop bucket add nerd-fonts
sudo scoop install -g CascadiaCode-NF
```

- Dalle impostazioni apri il file json e modifica le impostazioni così:

```sh
"defaults": 
{
    "fontFace": "CaskaydiaCove NF",
    "colorScheme": "One Half Dark",
    "useAcrylic": true,
    "acrylicOpacity": 0.85
}
```

## Installa Oh-My-Posh con incluso Posh-Git

- Installare oh-my-posh

  ```ps1
  winget install JanDeDobbeleer.OhMyPosh
  ```

- Impostare la shell (cambia [NOMEUTENTE])

  ```ps1
  oh-my-posh --init --shell pwsh --config C:\Users\[NOMEUTENTE]\AppData\Local\Programs\oh-my-posh\themes\pure.omp.json | Invoke-Expression
  ```

## Ricerca in history (PSReadline)

- Installare PSReadLine per Powershell Core (attenzione la 2.1.0-beta2 non fa il predictive intellisense)

  ```ps1
  Install-Module -Name PSReadLine -AllowPrerelease -Scope CurrentUser -Force -SkipPublisherCheck
  ```

- Imposta nuovo comportamento frecce su/giù (cercano nella history con prefisso già scritto, es. scrivo "get-" e premo freccia SU mostra altri comandi che iniziano con "get-", con Ctrl+f si attiva la ricerca)

  ```ps1
  Set-PSReadLineKeyHandler -Key UpArrow -Function HistorySearchBackward
  Set-PSReadLineKeyHandler -Key DownArrow -Function HistorySearchForward
  Set-PSReadLineKeyHandler -Key Ctrl+f -Function ReverseSearchHistory
  ```

## Salva le impostazioni nel profilo

- Salva le impostazioni nel profilo `notepad $PROFILE`

  ```ps1
  oh-my-posh --init --shell pwsh --config C:\Users\[NOMEUTENTE]\AppData\Local\Programs\oh-my-posh\themes\pure.omp.json | Invoke-Expression
  Import-Module posh-git
  Set-PSReadLineKeyHandler -Key UpArrow -Function HistorySearchBackward
  Set-PSReadLineKeyHandler -Key DownArrow -Function HistorySearchForward
  Set-PSReadLineKeyHandler -Key Ctrl+f -Function ReverseSearchHistory
  cls
  ```

- (opzionale) per nascondere l'utente di default modificare `C:\Users\[NOMEUTENTE]\AppData\Local\Programs\oh-my-posh\themes\pure.omp.json`

```json
--- "template": "{{ .UserName }} ",
+++ "template": "{{ if ne .UserName \"NOMEUTENTE\" }}{{ .UserName }}{{ end }} ",
```
