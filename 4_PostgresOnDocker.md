# Reinstall Windows - Postgres on Docker

## Create dev-postgres container

> Execute these from your default WSL distro

```sh
## 0. Pull postgres image
docker pull postgres:15.1

## 1. Create a folder in a known location for you
mkdir ${HOME}/postgres-data/

## 2. run the postgres image
  docker run \
      --name dev-postgres \
      -e POSTGRES_PASSWORD=postgres \
      -v ${HOME}/postgres-data/:/var/lib/postgresql/data \
      -p 5432:5432 \
      postgres:15.1
```

Now add a scheduled task to start automatically after 1 minute (otherwise postgres not respond until container restart).  
Here the example to import `E:\Utils\DelayedStarts\DelayedStartScheduledTask.xml`.

## Configure to enable no-password access from host

```sh
## 1. Edit pg_hba.conf
sudo vi ${HOME}/postgres-data/pg_hba.conf

## 2. Add all trusted rule and disable scram-sha-256
>>>
host    all             all             0.0.0.0/0               trust
# host all all all scram-sha-256

## 3. Restart container
docker restart dev-postgres
```

## Transfer DB from old to new Postgres

> From host machine

```cmd
cd "C:\Users\oscar\AppData\Local\Programs\pgAdmin 4\v6\runtime"
.\pg_dump.exe -h 127.0.0.1 -p 49156 -U postgres -C <DATABASE> | .\psql.exe -h 127.0.0.1 -p 5432 -U postgres
```

> From WSL2

```sh
sudo apt-get install postgresql-client
pg_dump -h 127.0.0.1 -p 49156 -U postgres -C <DATABASE> | psql -h 127.0.0.1 -p 5432 -U postgres
```

If version mismatch

```sh
> pg_dump: error: server version: 15.1 (Debian 15.1-1.pgdg110+1); pg_dump version: 14.5 (Ubuntu 14.5-0ubuntu0.22.04.1)
> pg_dump: error: aborting because of server version mismatch
```

Add postgres repo and install same version of postgres client

```sh
# as root
apt install curl ca-certificates gnupg

curl https://www.postgresql.org/media/keys/ACCC4CF8.asc | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/apt.postgresql.org.gpg >/dev/null

echo "deb http://apt.postgresql.org/pub/repos/apt jammy-pgdg main" > /etc/apt/sources.list.d/pgdg.list # `jammy` is the current ubuntu version, check it with `apt search postgres`

# as user
sudo apt update
sudo apt-get install postgresql-client-15 # 15 is the current docker version
```

## Backup database data

```sh
docker exec -it dev-postgres \
  pg_dump -U<user_name> --column-inserts --data-only <db_name> > \
  backup_data.sql
```

## (OPTIONAL) Use pgAdmin from docker container

```sh
docker pull dpage/pgadmin4
docker run \ 
    -p 20000:80 \
    -e 'PGADMIN_DEFAULT_EMAIL=oscar.locatelli@evoloop.it' \
    -e 'PGADMIN_DEFAULT_PASSWORD=postgres' \
    --name dev-pgadmin \ 
    -d dpage/pgadmin4
```

To connect pgAdmin to dev-postgres you have to use remote ip

```sh
docker inspect dev-postgres -f "{{json .NetworkSettings.Networks }}"
>>>
{"bridge":{"IPAMConfig":null,"Links":null,"Aliases":null,"NetworkID":"60c21f5cfcaaff424a0e4a22463dc8f9a285993de04e7ac19ce5fd96bba56a47","EndpointID":"be6e45b659c30bd12aa766d7003a2887607053684b68574e426f8823104b18a2","Gateway":"172.17.0.1","IPAddress":"172.17.0.2","IPPrefixLen":16,"IPv6Gateway":"","GlobalIPv6Address":"","GlobalIPv6PrefixLen":0,"MacAddress":"02:42:ac:11:00:02","DriverOpts":null}}
```
