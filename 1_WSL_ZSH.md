# INSTALLAZIONE WSL2 + ZSH + O-MY-ZSH SU WSL2

## INSTALLAZIONE WSL2 + GIT

Usare la WSL 2 per avere un ambiente Linux "nativo" (quasi) in Windows.
Useremo Git e Node direttamente da WSL2 (attenzione, usare solo file all'interno del file system di linux).
Con VS Code ci collegheremo direttamente alla WSL2 per modificare il codice.

### Installazione WSL2 e Distribuzione Linux

- **(NO WIN 11+)** Abilitare il Windows Subsystem for Linux dalle feature di Windows da pannello di controllo o da prompt elevato:

```sh
dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart
```

- **(NO WIN 11+)** Abilitare la Virtual Machine Platform dalle feature di Windows da pannello di controllo o da prompt elevato:

```sh
dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart
```

- Scaricare l'aggiornamento del kernel per WSL2 (se non si installa, riavviare)
  <https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_x64.msi>

- Aggiornare il WSL da 1 a 2

```sh
wsl --set-default-version 2
```

- Creare la configurazione globale per il rilascio delle risorse WSL

```sh
echo "[experimental]
autoMemoryReclaim=gradual" >> "$($env:USERPROFILE)\.wslconfig"
```

- Installa la distribuzione WSL preferita dal Microsoft Store cercando WSL (nella guida useremo Ubuntu, i comandi all'interno della WSL cambiano in base alla distro scelta)

- Aprire la WSL installata e una volta inizializzata inserire nome utente e password per creare le nuove credenziali

- Aggiornare la distro

```sh
sudo apt-get update
sudo apt-get upgrade
```

- Installa tool essenziali

```sh
sudo apt install build-essential
sudo apt install net-tools
```

## Installazione Git

- Installa Git

  - Su Windows: Scarica e installa git for windows da <https://git-scm.com/download/win> (non con scoop altrimenti alcuni file esterni non lo vedono)

    - Aggiungere al path di sistema durante l'installazione (rendere disponibile dal command prompt di windows)
    - Tutto il resto di default

  - Su Linux

```sh
sudo apt-get install git
```

- Eseguire i comandi per configurare GIT per lavorare in collaborazione da Powershell o CMD
  (salvataggio credenziali e fix per gestire il carattere di fine riga in modo che sia compatibile tra Windows e UNIX-like)

```sh
git config --global core.autocrlf input
git config --global credential.helper store
git config --global user.name "Nome e cognome"
git config --global user.email email@example.com
```

- Alternativa al salvataggio delle credenziali per sempre è possibile salvarle in cache (al posto di git config --global credential.helper ...)

```sh
# 15 Minuti:
git config --global credential.helper cache

# Timeout specificato (in secondi):
git config --global credential.helper 'cache --timeout=3600'
```

- (opzionale) Impostare Vim come editor predefinito per git, su Ubuntu è preimpostato Nano

```sh
git config --global core.editor "vim"
```

- Visualizzazione più compatta dei log e albero log

```sh
# Imposta il git log in modalita oneline con autori, date relative e colori
git config --global format.pretty "format:%C(auto,yellow)%h %C(auto,blue)%>(12,trunc)%ar %C(auto,green)%<(15,trunc)%aN %C(auto,reset)%s%C(auto)%d"

# Crea l'alias git fulllog con log in formato standard
git config --global alias.fulllog "log --pretty --date iso"

# Crea l'alias git work con log ad albero con tutti i branch
git config --global alias.graph "log --pretty=format:'%C(auto,yellow)%h %C(auto,blue)%>(12,trunc)%ar %C(auto,green)%<(15,trunc)%aN%C(auto)%d %C(reset) %s' --graph --all"
```

- Crea la tua chiave SSH per l'autenticazione a GIT (oppure copiati le tue chiavi) e aggiungi la .pub per l'autenticazione su Github/Gitlab/...

```sh
ssh-keygen -t rsa -b 4096 -C "la tua email"
# conferma i path predefiniti
```

### Personalizzazione Windows Terminal Preview

- Apri il Windows Terminal Preview
- Cambia la shell predefinita dalle impostazioni nella tua WSL2 e l'aspetto con il tema preferito
- Installa la versione Nerd Font di Cascadia Code, CaskaydiaCove NF

```sh
scoop bucket add nerd-fonts
sudo scoop install -g CascadiaCode-NF
```

- Dalle impostazioni apri il file json e modifica le impostazioni così:

```sh
 ...
"defaults":
{
    "fontFace": "CaskaydiaCove NF",
    "colorScheme": "One Half Dark",
    "useAcrylic": true,
    "acrylicOpacity": 0.85,
} ...
```

- Se non si apre nella home directory va impostata a mano aggiungendo `startingDirectory`

```sh
 ...
"list":
[
    {
        "guid": "[NON MODIFICARE]",
        "hidden": false,
        "name": "Ubuntu-[VERSIONE DI UBUNTU]",
        "source": "Windows.Terminal.Wsl",
        "startingDirectory": "\\\\wsl$\\Ubuntu-[VERSIONE DI UBUNTU]\\home\\[NOME UTENTE]"
    },
    ...
] ...
```

### Installazione ZSH su WSL2

```sh
sudo apt install zsh
zsh # e poi segui gli step
```

#### Installazione OH-MY-ZSH e personalizzare il prompt

- Installare Oh-My-ZSH

```sh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
```

- Installa i plugin aggiuntivi

```sh
cd ~/.oh-my-zsh/custom/plugins
git clone https://github.com/zsh-users/zsh-syntax-highlighting
git clone https://github.com/zsh-users/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-completions
```

- Installare il tema Powerlevel10k: <https://github.com/romkatv/powerlevel10k>

```sh
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k
```

- Imposta `ZSH_THEME="powerlevel10k/powerlevel10k"` in `~/.zshrc`

- Ricarica `zsh` partirà la configurazione, altrimenti `p10k configure` e configura a piacimento, imposta `lean` come stile per avere tutte le feature oppure `pure` per conformità con la parte windows

#### Configurazione ZSH

- Modifica il file `~/.zshrc`

```sh
plugins=(
        git
        git-prompt
        colored-man-pages
        zsh-syntax-highlighting
        zsh-autosuggestions
        zsh-completions
)
```

- Aggiorna lista auto completamento zsh-completions

```sh
autoload -U compinit && compinit
```
