# Reinstall Windows - SQL Server on Docker

## Create dev-mssql container

> Execute these from your default WSL distro

```sh
## 0. Pull postgres image
docker pull mcr.microsoft.com/mssql/server:2022-latest

## 1. Create a folder in a known location for you
mkdir ${HOME}/mssql-data/

## 2. Check user id (from container logs on docker for windows) and set as volume folder owner
docker run -it --rm --entrypoint /bin/sh mcr.microsoft.com/mssql/server:2022-latest -c "whoami | id -u"
> 10001

sudo chown 10001 ${HOME}/mssql-data

## 2. run the postgres image
docker run \
    --name dev-mssql \
    --hostname dev-mssql \
    -e "ACCEPT_EULA=Y" \
    -e "MSSQL_SA_PASSWORD=Mssql22$" \
    -v ${HOME}/mssql-data/:/var/opt/mssql/data \
    -p 1433:1433 \
    -d \
    mcr.microsoft.com/mssql/server:2022-latest
```
