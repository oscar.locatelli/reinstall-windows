# Reinstall Windows - Docker Dev Tools

## Create dev-maildev container

> Execute these from your default WSL distro

```sh
## 0. Pull postgres image
docker pull maildev/maildev

## 1. run the maildev image
docker run \
    --name dev-maildev \
    -p 1080:1080 \
    -p 1025:1025 \
    -d \
    maildev/maildev
```
