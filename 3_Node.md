# Reinstall Windows - Node

## Node

```sh
winget install CoreyButler.NVMforWindows
# Restart powershell
nvm list available
nvm install LATEST_LTS_VERSION
sudo nvm use LATEST_LTS_VERSION
npm i -g pnpm
npm i -g yarn
```