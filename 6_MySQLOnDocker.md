# Reinstall Windows - MySQL on Docker

## Create dev-mysql container

> Execute these from your default WSL distro

```sh
## 0. Pull postgres image
docker pull mysql:5.7

## 1. Create a folder in a known location for you
mkdir ${HOME}/mysql-data/

## 2. run the postgres image
docker run \
    --name dev-mysql \
    -p 3306:3306 \
    -e "MYSQL_ROOT_PASSWORD=mysql" \
    -v ${HOME}/mysql-data/:/var/lib/mysql \
    -d \
    mysql:5.7
```
