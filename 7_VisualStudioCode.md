# Guida all'installazione di visual Studio 2022

## Visual Studio

- Installa l'ultimo VS Code con Winget:

  ```sh
  winget install Microsoft.VisualStudioCode
  ```

- Abilita i profili se fosse necessario e importa i seguenti profili da GIST Github:

  - `Default`: <https://vscode.dev/profile/github/1142f16c80bdcd846e5502efc76bf196>
  - `react`: <https://vscode.dev/profile/github/e10d8d16e8e9cbd50c2d4ae26e2f2800>
  - `vue`: <https://vscode.dev/profile/github/0e44cfc7d5d6812560cc849ed00ed463>

> Esempio di configurazione importata in `/VSCode/settings.json`.
