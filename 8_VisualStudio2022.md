# Guida all'installazione di visual Studio 2022

## Visual Studio

- Scaricare Visual Studio 2022 Community Edition: <https://visualstudio.microsoft.com/it/downloads/>

### Workload

- Sviluppo ASP.NET e Web
- Sviluppo Azure
- Elaborazione ed archiviazione dati

- Installare il workload "Desktop development with C++" con Windows 10 SDK al posto di Windows 11 SDK per poter utilizzare node-gyp.

### Singoli componenti (aggiungere)

- .NET Core 3.1 Runtime
- .NET Core 6.0 Runtime
- .NET Framework 4.8 SDK + Targeting Pack
- .NET Framework 4.7.2 Targeting Pack
- Funzionalità avanzate ASP.NET
- Windows 11 SDK (RIMUOVERE)
- Convalida delle dipendenze
- Editor DGML
- Progettazione classi

### Language Pack

- Aggiungere Inglese e togliere Italiano

## Estensioni

Installare queste estensioni:

- Web Compiler 2022+: _compilatore assets (per retrocompatibilità)_
- Bundle & Minifier 2022+: _compilatore assets (per retrocompatibilità)_
- Image Optimizer: _ottimizza immagini_
- NPM Task Runner: _permette di automatizzare gli script npm_
- MultiLinter 2022: _abilita all'utilizzo di linter lato client (nella configurazione sotto imposteremo eslint e prettier)_
- EF Core Power Tools: _Aggiunge funzionalità all'IDE per EnittyFramework Core_
- SonarLint for Visual Studio 2022: _Trova possibili errori nella scrittura del codice (Code Smells)_

Opzionali:

- Dracula: _tema scuro molto ben fatto e presente anche per VS Code per mantenere la stessa UI/UX in entrambi gli ambienti_
- OneDarkPro: _altro tema scuro molto ben fatto in alternativa_

## Configurazione

### Prerequisiti

- Per progetti vecchi che utilizzano SQL LocalDB 2012 installare `\\nas\Dev\Software\Sql Server\SQL SERVER 2012\SqlLocalDB.MSI`

- Installa Node.js l'ultima LTS tramite NVM

```sh
winget install CoreyButler.NVMforWindows
# Restart powershell
nvm list available
nvm install LATEST_LTS_VERSION
sudo nvm use LATEST_LTS_VERSION
npm i -g pnpm
npm i -g yarn
```

- Installa Yarn e pnpm in global

  ```sh
  npm i -g yarn
  npm i -g pnpm
  ```

- Aumentare la memoria massima per l'heap di Node.js creando una variabile d'ambiente `NODE_OPTIONS` con valore `--max-old-space-size=8192`.  
  Da Windows - Sistema - Impostazioni avanzate - Variabili d'ambiente.

### Generali

- Menu -> Tools -> Options -> Project and Solutions -> Web Package Management -> External Web Tools -> Aggiungere il path `$(NVM_SYMLINK)` e posizionarlo appena SOPRA (prima) di `$(VSInstalledExternalTools)` (se si hanno problemi con PNPM all'interno vi VS spostare anche `$(PATH)` prima di `$(VSInstalledExternalTools)`)

- Menu -> Tools -> Options -> Text Editor -> Disabilita "Enable mouse click to perform Go to Definition"

- Nella ricerca tra i file (Ctrl+Shift+F) impostare in FileTypes questa stringa per evitare di cercare nei compilati, nei minificati e nelle librerie:
  `!*\bin\*;!*\obj\*;!*\.*;!*\node_modules\*;!*\wwwroot\lib\*;!*\App_Structure\lib\*;!*.min.css;!*.min.js;!*\pnpm-lock.yaml;!*\package-lock.json;!*\yarn.lock`

### MultiLinter

Configura VS per l'utilizzo di prettier come linter per tutti gli assets lato client.

Ognuno di questi file verrà formattato al salvataggio con regole standard de facto internazionali.

- Menu -> Tools -> Options -> Text Editor -> Javascript/Typescript -> Linting -> General: Disattivare tutto

- Menu -> Tools -> Options -> MultiLinter 2022 -> General: Abilita tutto e imposta tutto su 'On Save' (tranne la debug mode)

- Menu -> Extensions -> Multilinter 2022: Salva il contenuto sottostante (DA NON CAMBIARE PER NON CONTINUARE A SOVRASCRIVERSI A VICENDA IN GIT)

```json
{
  "eslint": {
    "enabled": true,
    "additionalArguments": "--fix --ignore-path .gitignore --ignore-path .prettierignore",
    "installationType": "local",
    "fileExtensions": "js,jsx,ts,vue"
  },
  "prettier": {
    "enabled": true,
    "installationType": "local",
    "fileExtensions": "js,jsx,ts,vue,css,scss,less,json,yaml,md,html"
  }
}
```

### Configurazione toolchain e riconfigurazione vecchi progetti ASP.NET WebForm

La nuova toolchain di bundler (basata su Gulp) permette di utilizzare codice ES6+ nei file .js e file di tipo .vue.

Per migrare i vecchi progetti servono alcuni passi (attenzione potrebbero variare in base alla struttura del progetto)

- Estrarre `\\nas\Dev\Software\Visual Studio\GulpBundler.zip` nella root del progetto Web, che contiene:

  - `.prettierrc.js` e `.prettierignore` per configurare il formatter

  - `.eslintrc.js` per configurare il linter JS

  - `gulpfile.json` per configurare il bundler

  - `package.json` e `pnpm-lock.yaml` per installare tutti i pacchetti necessari ai tre tool qui sopra

  - `bundles.json` per configurare i bundle js/css/... da compilare, è un esempio da sistemare, la sintassi è molto simile al vecchio `bundleconfig.json`

- Eliminare i file `compilerconfig.json` e `compilerconfig.json.defaults` (non usiamo piú Web Compiler)

- Eliminare i file `bundleconfig.json` e `bundleconfig.json.bindings` (non usiamo più Bundle & Minifier)

- Finita l'installazione dei pacchetti chiudi e riapri VS

- Dovrebbe aprirsi il Task Runner Explorer con il job `auto` di package.json attivo, altrimenti Menu -> View -> Other Windows -> Task Runner Explorer (Gulp partirà ad ogni apertura di VS in watch sui file modificati e li compilerà al salvataggio)

- SOLO PER WEB SITE ASP.NET WEBFORM: modificare le proprietà della cartella node_modules impostandola come NASCOSTA per evitare che Visual Studio cerchi di compilarne l'interno

- PER PROGETTI ESISTENTI applicare le regole di eslint e prettier su tutti i file e controllare che non tocchi file errati (eventualmente aggiornare .prettierignore):

  - Accedere alla cartella di root del progetto Web

  - Fare un commit preventivo se ci sono modifiche per controllare successivamente quello che viene modificato dai prossimi due comandi

  - Lanciare il linter e il formatter

    ```sh
    npx eslint -f json --fix --ignore-path .gitignore --ignore-path .prettierignore .
    npx prettier --write .
    ```
