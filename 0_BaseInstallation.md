# Reinstall Windows - Base Installation

## Base

```sh
winget install wingetui
winget install Google.Chrome
winget install Mozilla.Firefox
winget install git.git
winget install voidtools.Everything
winget install OpenVPNTechnologies.OpenVPN
winget install Fortinet.FortiClientVPN
winget install NTop
```

```sh
notepad $PROFILE
```

Aggiungi queste righe:

```sh
Set-Alias top ntop
cls
```

Chiudi e riapri la Powershell.

### Tastiera US

```sh
winget install lexikos.autohotkey # Copia link in Startup di `E:\Utils\AutoHotkey\us-keyboard-accents.ahk`
```

## Package manager

### Scoop

```sh
Set-ExecutionPolicy RemoteSigned -Scope CurrentUser # Optional: Needed to run a remote script the first time
irm get.scoop.sh | iex

# Aggiungi la cartella di scoop ai repository affidabili
git config --global --add safe.directory C:/Users/oscar/scoop/apps/scoop/current
git config --global --add safe.directory C:/Users/oscar/scoop/apps/scoop/bucket

scoop bucket add extras

scoop install sudo           # Permette di utilizzare sudo per lanciare comandi come amministratore
scoop install curl           # Url Data Transfer Tool
scoop install curlie         # Curl formattato
scoop install wget           # HTTP Client
scoop install telnet         # Telnet client
scoop install bat            # cat formattato: visualizza file di testo formattato e paginato
scoop install less           # Visualizzazione file di testo con paginazione
scoop install grep           # Cerca stringhe in un file
scoop install ripgrep        # Grep formattato: rg
scoop install dust           # Dimensione cartelle e file
scoop install duf            # Utilizzo disco e unità di rete
scoop install dog            # DNS Query Tool
scoop install posh-git       # Git commands autocomplete
```

### Chocolatey

```sh
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))
```

## Lib

```sh
winget install Python.Python.3.11
winget install Microsoft.VCRedist.2015+.x64
# NO SE INSTALLI VS2022 winget install Microsoft.VisualStudio.2022.BuildTools
winget install Oracle.JavaRuntimeEnvironment
```

## Dev

```sh
winget install microsoft.powershell
# NO WIN 11 winget install 9N0DX20HK701 # Windows Terminal
winget install 9N8G5RFZ9XK3 # Windows Terminal Preview
winget install SublimeHQ.SublimeText.4 # Execute as admin `E:\Utils\Sublime-Text-Right-Menu.bat`
winget install Microsoft.DotNet.SDK.8 # And others
winget install docker.dockerdesktop
winget install Microsoft.VisualStudioCode
winget install winscp.winscp
sudo choco install kitty # Add full winscp path on kitty.ini
sudo choco install insomnia.insomnia
winget install PostgreSQL.pgAdmin
winget install dbeaver.dbeaver
winget install Microsoft.SQLServerManagementStudio
```

### Extra Dev

```sh
winget install ngrok.ngrok
winget install meld.meld
```

Per ambienti vecchi (es. Nuxt 2)

```sh
winget install Python.Python.2
# winget install Microsoft.VisualStudio.2017.BuildTools
winget install Microsoft.BuildTools2015 # 2017 rimosso, installiamo il 2015
```

Add `C:\Python27` to Path and rename `python.exe` into `python2.exe` to use side by side to Python 3.

## Comunicazione

```sh
winget install discord.discord
winget install Microsoft.Teams
winget install Telegram.TelegramDesktop
```

## Utilità

```sh
winget install Microsoft.PowerToys
sudo choco install DevToys
winget install dropbox.dropbox
winget install Google.Drive
winget install calibre.calibre
winget install lencx.ChatGPT
winget install Figma.Figma
winget install Greenshot.Greenshot
winget install iLovePDF.iLovePDFDesktop
winget install qbittorrent.qbittorrent
winget install Sandboxie.Plus
```

For calibre add DeDRM plugin from here and create a new eInk Kindle Key with kindle serial number (get it from amazon.it in Device List):
<https://github.com/apprenticeharper/DeDRM_tools/releases>
